package ru.inordic.ivasileva.list;

import java.util.LinkedList;

public class MyList {
    private LinkedList<Integer> integerList;

//    public MyList(LinkedList<Integer> integerList) {
//        this.integerList = integerList;
//    }


    public LinkedList<Integer> getIntegerList() {
        return integerList;
    }

    public void setIntegerList(LinkedList<Integer> integerList) {
        this.integerList = integerList;
    }

    public void addElementByIndex(int index, int element) {
        integerList.add(index, element);
    }

    public void addElementLast(int element) {
        integerList.addLast(element);
        System.out.println("last index " + integerList.getLast().toString());
    }


    public void deleteElement(int index) {
        integerList.remove(index);
    }

    public int getSizeList() {
        return integerList.size();
    }

    public int getElementValue(int index) {
        return integerList.get(index);
    }
}
