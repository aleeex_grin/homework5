package ru.inordic.ivasileva.list;

import java.util.Scanner;

public class IntegerList {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        MyList list = new MyList();
        boolean isCorrect = false;
        int action;

        try {
            do {
                if (isCorrect) {
                    System.out.println("Please enter a valid number between 0 and 3");
                }
                System.out.println(String.join("\r\n", "Select the action: ", "0 - add element",
                        "1 - remove element", "2 - get size list", "3 - get value by index of the list", "4 - exit"));

                action = scanner.nextInt();
                isCorrect = action > 3;
            }
            while (isCorrect);

            switch (action) {
                case 0:
                    System.out.println("Введите число:");
                    int number = scanner.nextInt();
                    System.out.println("Выберите следующее:" + "\r\n" +
                            "0 - Добавить число в конец списка" + "\r\n" +
                            "1 - Добавить число в позицию");
                    int operationZeroCase = scanner.nextInt();

                    if (operationZeroCase == 0) {
                        list.addElementLast(number);
                    } else if (operationZeroCase == 1) {
                        System.out.println("Enter a index");
                        int inputIndex = scanner.nextInt();
                        list.addElementByIndex(inputIndex, number);
                        System.out.println("show the list " + list.getSizeList() + " " );
                        //todo: do-while
                    } else {
                        System.out.println("Enter a valid operation number");
                    }
                    break;

                case 1:

                    break;

                case 2:
                    break;

                case 3:
                    break;

                default:
            }

        } catch (Exception e) {
        }

        scanner.close();
    }
}
